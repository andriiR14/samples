package org.andrii.lock;

import java.util.ArrayList;
import java.util.List;

public class Processor {

    private List<Integer> list;
    private static int LIMIT = 5;
    private static int BOTTOM = 0;
    private int val;

    public Processor() {
        list = new ArrayList<>();
    }

    public void produce() throws InterruptedException {
        synchronized (list) {
            while (true) {
                if (list.size() == LIMIT) {
                    System.out.println("waiting for removing element");
                    list.wait();
                } else {
                    System.out.println("Adding element to list: " + val);
                    list.add(val++);
                    list.notify();
                }
                Thread.sleep(1000);
            }
        }
    }

    public void consume() throws InterruptedException {
        synchronized (list) {
            while (true) {
                if (list.size() == BOTTOM) {
                    System.out.println("waiting for adding element");
                    list.wait();
                } else {
                    System.out.println("removing element " + list.remove(--val) + " from list");
                    list.notify();
                }
                Thread.sleep(1000);
            }
        }
    }
}

package org.andrii.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SampleLock {
    private static Lock lock = new ReentrantLock();
    private static int counter = 0;

    public static void invrement() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                counter++;
            }
        } finally {
            lock.unlock();
        }
    }

    public static int getCounter() {
        return counter;
    }
}

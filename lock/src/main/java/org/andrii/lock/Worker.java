package org.andrii.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Worker {
    private List<Integer> list = new ArrayList<>();
    private Lock lock = new ReentrantLock();
    private Condition fullCondition = lock.newCondition();
    private Condition emptyCondition = lock.newCondition();
    private int LIMIT = 5;
    private int val;

    public void consume() throws InterruptedException {
        try {
            lock.lock();

            while (true) {
                if (list.size() == 0) {
                    System.out.println("waiting for adding element");
                    emptyCondition.await();
                } else {
                    System.out.println("removing element " + list.remove(--val) + " from list");
                    fullCondition.signal();
                }
                Thread.sleep(1000);
            }
        } finally {
            lock.unlock();
        }
    }

    public void produce() throws InterruptedException{
        try {
            lock.lock();
            while (true) {
                if (list.size() == LIMIT) {
                    System.out.println("waiting for removing element");
                    fullCondition.await();
                } else {
                    System.out.println("Adding element to list: " + val);
                    list.add(val++);
                    emptyCondition.signal();
                }
                Thread.sleep(1000);
            }
        } finally {
            lock.unlock();
        }
    }
}
